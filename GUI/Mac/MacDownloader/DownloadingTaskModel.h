//
//  DownloadingTaskModel.h
//  MacDownloader
//
//  Created by Xu Xu on 1/17/14.
//  Copyright (c) 2014 Xu Xu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DownloadingTaskModel : NSObject

@property (nonatomic, readwrite) NSString * filename;
@property (nonatomic, readwrite) NSString * filepath;
@property (nonatomic, readwrite) NSNumber * percentage;
@property (nonatomic, readwrite) int task;

@end
