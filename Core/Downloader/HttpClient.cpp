/*****************************************************************************
 Copyright (c) 2014 Xu Xu (Vivo)
 
 http://git.xuxu.name/downloader
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HttpClient.h"
using namespace std;

#pragma mark - Constructor & Destructor

HttpClient::HttpClient() : m_onProgress(nullptr), m_lastResponseCode(0)
{
    initCURL();
}

HttpClient::HttpClient(string agent) : HttpClient()
{
    curl_easy_setopt(m_curl, CURLOPT_USERAGENT, agent.c_str());
}


HttpClient::~HttpClient()
{
    curl_easy_cleanup(m_curl);
}

#pragma mark - Settings

void HttpClient::setProgressCallback(HttpClientProgressCallback callback)
{
    m_onProgress = callback;
    
    if (m_onProgress != nullptr)
    {
        // set up curl progress function
        curl_easy_setopt(m_curl, CURLOPT_NOPROGRESS, 0L);
        curl_easy_setopt(m_curl, CURLOPT_PROGRESSFUNCTION, &HttpClient::receivedProgress);
        curl_easy_setopt(m_curl, CURLOPT_PROGRESSDATA, this);
    }
    else
    {
        // no progress
        curl_easy_setopt(m_curl, CURLOPT_NOPROGRESS, 1L);
    }
}

#pragma mark - Network Requests

int HttpClient::get(string url)
{
    // set url
    curl_easy_setopt(m_curl, CURLOPT_URL, url.c_str());
    
    // forward all data to this func
    curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, &HttpClient::writeToString);
    
    curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &m_lastResponseBody);
    
    perform();
    
    curl_easy_getinfo(m_curl, CURLINFO_RESPONSE_CODE, &m_lastResponseCode);
    
    return m_lastResponseCode;
}

int HttpClient::get(string url, map<string, string> &query)
{
    // TODO: replace :param in url in future
    
    string params;
    
    for (auto pair : query)
    {
        if (!pair.second.empty())
        {
            char* escapedValue = curl_easy_escape(m_curl, pair.second.c_str(), (int)pair.second.length());
            
            if (!params.empty())
            {
                params += "&";
            }
            
            params += pair.first + "=" + escapedValue;
            
            curl_free(escapedValue);
        }
    }
    
    if (!params.empty())
    {
        url += (url.find("?") == string::npos ? "?" : "&") + params;
    }
    
#ifdef DEBUG_MODE
    printf("VVV: new url is: %s\n", url.c_str());
#endif
    
    return get(url);
}

int HttpClient::getWithHeader(string url, string header)
{
    // prepare the header
    struct curl_slist *slist = NULL;
    slist = curl_slist_append(slist, header.c_str());
    
    curl_easy_setopt(m_curl, CURLOPT_HTTPHEADER, slist);
    
    get(url);
    
    // clean up
    curl_slist_free_all(slist);
    
    return m_lastResponseCode;
}

int HttpClient::post(string url, const map<string, string> &strings)
{
    map<string, string> files;
    return post(url, strings, files);
}

int HttpClient::post(string url, const map<string, string> &strings, const map<string, string> &files)
{
    multimap<string, string> ms;
    for (auto i = strings.begin(); i != strings.end(); ++i)
    {
        ms.insert(*i);
    }
    multimap<string, string> fs;
    for (auto i = files.begin(); i != files.end(); ++i)
    {
        fs.insert(*i);
    }
    return post(url, ms, fs);
}

int HttpClient::post(string url, const multimap<string, string> &strings, const multimap<string, string> &files)
{
    // set url
    curl_easy_setopt(m_curl, CURLOPT_URL, url.c_str());
    
    // setup post form
    struct curl_httppost * form = NULL;
    struct curl_httppost * lastptr = NULL;
    for (auto & p : strings)
    {
        curl_formadd(&form,
                     &lastptr,
                     CURLFORM_COPYNAME, p.first.c_str(),
                     CURLFORM_COPYCONTENTS, p.second.c_str(),
                     CURLFORM_END);
    }
    for (auto & p : files)
    {
        curl_formadd(&form,
                     &lastptr,
                     CURLFORM_COPYNAME, p.first.c_str(),
                     CURLFORM_FILE, p.second.c_str(),
                     CURLFORM_END);
    }
    
    curl_easy_setopt(m_curl, CURLOPT_HTTPPOST, form);
    
    // forward all data to this func
    curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, &HttpClient::writeToString);
    
    curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &m_lastResponseBody);
    
    perform();
    
    curl_easy_getinfo(m_curl, CURLINFO_RESPONSE_CODE, &m_lastResponseCode);
    
    // clean up the form post chain
    curl_formfree(form);
    
    return m_lastResponseCode;
}

int HttpClient::post(string url, string content)
{
    // set url
    curl_easy_setopt(m_curl, CURLOPT_URL, url.c_str());
    
    // set post content
    curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, content.c_str());
    
    // forward all data to this func
    curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, &HttpClient::writeToString);
    
    curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &m_lastResponseBody);
    
    perform();
    
    curl_easy_getinfo(m_curl, CURLINFO_RESPONSE_CODE, &m_lastResponseCode);
    
    return m_lastResponseCode;
}

int HttpClient::postWithHeaders(string url, string content, vector<string> headers)
{
    // prepare the header
    struct curl_slist *slist = NULL;
    for (auto s : headers)
    {
        slist = curl_slist_append(slist, s.c_str());
    }
    
    curl_easy_setopt(m_curl, CURLOPT_HTTPHEADER, slist);
    
    post(url, content);
    
    // clean up
    curl_slist_free_all(slist);
    
    return m_lastResponseCode;
}

int HttpClient::getResponseCode()
{
    return m_lastResponseCode;
}

string HttpClient::getResponseBody()
{
    return m_lastResponseBody;
}

bool HttpClient::download(string url, string folder)
{
    // try guess filename from the url
    auto pos = find(url.rbegin(), url.rend(), '/');
    string name = url.substr(distance(pos, url.rend()));
    
    if (folder.back() != kPathSeparator) folder += kPathSeparator;
    
    return downloadAs(url, folder + name);
}

bool HttpClient::downloadAs(string url, string filepath)
{
    // set url
    curl_easy_setopt(m_curl, CURLOPT_URL, url.c_str());
    
    // forward all data to this func
    curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, &HttpClient::writeToFile);
    
    // try to write a temp file instead of the real file
    string tmpFile = filepath + ".tmp";
    
    // open the file
    FILE * file = fopen(tmpFile.c_str(), "wb");
    if (file)
    {
        // write the page body to this file handle. CURLOPT_FILE is also known as CURLOPT_WRITEFILE
        curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, file);
        
        // do it
        bool isSuccessful = perform();
        
        fclose(file);
        
        // get HTTP response code
        m_lastResponseBody = "";
        curl_easy_getinfo(m_curl, CURLINFO_RESPONSE_CODE, &m_lastResponseCode);
        
        if (isSuccessful && m_lastResponseCode == HttpStatus::OK) // only assume 200 is correct
        {
            // rename it to real filename
            rename(tmpFile.c_str(), filepath.c_str());
            return true;
        }
        
        remove(tmpFile.c_str()); // clean tmp file
    }
    
    return false;
}

#pragma mark - Curl callback functions

size_t HttpClient::writeToFile(void * ptr, size_t size, size_t nmemb, FILE * stream)
{
    return fwrite(ptr, size, nmemb, stream);
}

size_t HttpClient::writeToString(char * ptr, size_t size, size_t nmemb, string * sp)
{
    size_t len = size * nmemb;
    sp->insert(sp->end(), ptr, ptr + len);
    return len;
}

int HttpClient::receivedProgress(HttpClient *me, double downTotal, double downNow, double upTotal, double upNow)
{
    if (me->m_onProgress != nullptr)
    {
        me->m_onProgress(downTotal, downNow, upTotal, upNow);
    }
    return 0;
}

#pragma mark - Private Helper

void HttpClient::initCURL()
{
    // init global
    static bool globalInitialized = false;
    if (!globalInitialized)
    {
        curl_global_init(CURL_GLOBAL_ALL); // there won't be curl_global_cleanup();
        globalInitialized = true;
    }
    
    // init current session
    m_curl = curl_easy_init();
    
    //    curl_easy_setopt(m_curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);	// support basic, digest, and NTLM authentication
    // try not to use signals
    curl_easy_setopt(m_curl, CURLOPT_NOSIGNAL, 1L);
    
    // set a default user agent
    curl_easy_setopt(m_curl, CURLOPT_USERAGENT, curl_version());
    
    // set connection timeout in seconds
    curl_easy_setopt(m_curl, CURLOPT_CONNECTTIMEOUT, kConnectionTimeout);
    
#ifdef DEBUG_MODE
    // Switch on full protocol/debug output while testing
    curl_easy_setopt(m_curl, CURLOPT_VERBOSE, 1L);
    
    // disable progress meter, set to 0L to enable and disable debug output
    curl_easy_setopt(m_curl, CURLOPT_NOPROGRESS, 1L);
#endif
}

bool HttpClient::perform()
{
    // auto retry for kAutoRetryTimes times until OK
    for (int i = 0; i < kAutoRetryTimes; ++i)
    {
        CURLcode result = curl_easy_perform(m_curl);
        if (result == CURLE_OK)
        {
            return true;
        }
#ifdef DEBUG_MODE
        if (i + 1 < kAutoRetryTimes)
        {
            printf("VVV: Retry because of error: %d\n", result);
        }
#endif
    }
    return false; // return errorcode if only when needed
}
