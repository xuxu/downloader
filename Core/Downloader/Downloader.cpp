/*****************************************************************************
 Copyright (c) 2014 Xu Xu (Vivo)
 
 http://git.xuxu.name/downloader
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "Downloader.h"
#include "HttpClient.h"
using namespace std;

#pragma mark Static define and Constructors

Downloader * Downloader::instance()
{
    static Downloader * s_instance = NULL;
    
    if (s_instance == NULL)
    {
        s_instance = new Downloader;
    }
    
    return s_instance;
}

Downloader::Downloader() : m_lastId(0), m_threadCount(0), m_onCompleted(nullptr)
{
}

Downloader::~Downloader()
{
}

#pragma mark - Public Methods

void Downloader::setCompletedCallback(DownloaderCallback callback)
{
    m_onCompleted = callback;
}

int Downloader::download(string url, string folder)
{
    return download(url, folder, "");
}

int Downloader::download(string url, string folder, DownloaderCallback callback)
{
    return download(url, folder, "", callback);
}

int Downloader::download(string url, string folder, string filename)
{
    return download(url, folder, filename, nullptr);
}

int Downloader::download(string url, string folder, string filename, DownloaderCallback callback)
{
    Task task;
    task.id = ++m_lastId;
    task.url = url;
    task.folder = folder;
    task.filename = filename;
    task.callback = callback;
    
    // put it in to the queue
    {
        lock_guard<mutex> lock(m_waitingMutex);
        m_waiting.push(task);
    }
    
    // init Status info
    {
        lock_guard<mutex> lock(m_statusMutex);
        m_status[task.id] = task.makeStatus();
    }
    
    // check thread count
    lock_guard<mutex> lock(m_threadCountMutex);
    if (m_threadCount < kMaxDownloadThreads)
    {
        ++m_threadCount;
        
        thread th(&Downloader::runNextTask, this);
        th.detach();
    }
    
    return task.id;
}

Downloader::Status Downloader::getStatus(int id)
{
    lock_guard<mutex> lock(m_statusMutex);
    return m_status[id];
}

#pragma mark -

// caution: this method will be called in threads!
void Downloader::runNextTask()
{
    HttpClient client;
    
    // alwasy looking for new task to execute
    while (true)
    {
        Task task;
        
        // try to fetch next task
        {
            lock_guard<mutex> lock(m_waitingMutex);
            if (!m_waiting.empty())
            {
                task = m_waiting.front();
                m_waiting.pop();
            }
        }
        
        if (task.id == 0) break; // no new task
        
        // remember it
        {
            lock_guard<mutex> lock(m_runningMutex);
            m_running[task.id] = this_thread::get_id();
        }
        
        // change status
        {
            lock_guard<mutex> lock(m_statusMutex);
            m_status[task.id].status = Status::RUNNING;
            m_status[task.id].startTime = time(NULL);
            
            // trace progress
            client.setProgressCallback([&](double t, double n, double, double){
                // update progress
                lock_guard<mutex> lock(m_statusMutex);
                m_status[task.id].totalSize = t;
                m_status[task.id].downloadedSize = n;
            });
        }
        
        // use httpclient to download it
        if (task.filename.empty())
        {
            client.download(task.url, task.folder);
        }
        else
        {
            client.downloadAs(task.url, task.folder + kPathSeparator + task.filename);
        }
        
        // change status
        {
            lock_guard<mutex> lock(m_statusMutex);
            m_status[task.id].status = Status::FINISHED;
            m_status[task.id].finishTime = time(NULL);
        }
        
        // remove me from the running list
        {
            lock_guard<mutex> lock(m_runningMutex);
            m_running.erase(task.id);
        }
        
        callCallbackSafely(task.callback, task.id);
    }
    
    // at last, minus thread count
    bool allDone;
    {
        lock_guard<mutex> lock(m_threadCountMutex);
        allDone = --m_threadCount == 0;
    }
    
    if (allDone) callCallbackSafely(m_onCompleted, 0);
}

void Downloader::callCallbackSafely(DownloaderCallback callback, int p)
{
    if (callback == nullptr) return;
    
    // make sure the callbacks are called one by one
    lock_guard<mutex> lock(m_callbackMutex);
    callback(p);
}
