/*****************************************************************************
 Copyright (c) 2014 Xu Xu (Vivo)
 
 http://git.xuxu.name/downloader
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#ifndef __Downloader__Downloader__
#define __Downloader__Downloader__

#include "Common.h"
#include <thread>
#include <mutex>

/**
 @param: int: task id
 */
typedef std::function<void(int)> DownloaderCallback;

class Downloader
{
public:
    /**
     Description of a downloading status
     */
    struct Status
    {
        int id;
        std::string url;
        std::string folder;
        std::string filename;
        time_t startTime, finishTime;
        double totalSize;
        double downloadedSize;
        enum {WAITING, RUNNING, PAUSED, FINISHED} status;
    };
    
    static Downloader * instance();
    virtual ~Downloader();
    
    /**
     Setup a callback function. When all tasks are done, call it.
     */
    void setCompletedCallback(DownloaderCallback callback);
    
    /**
     Download a file from url.
     @return: task ID. If failed, return -1
     */
    int download(std::string url, std::string folder);
    int download(std::string url, std::string folder, DownloaderCallback callback);
    int download(std::string url, std::string folder, std::string filename);
    int download(std::string url, std::string folder, std::string filename, DownloaderCallback callback);
    
    /**
     Get the status of a downloading task
     */
    Status getStatus(int id);
    
protected:
    /**
     Description of a download task
     */
    struct Task
    {
        int id;
        std::string url;
        std::string folder;
        std::string filename;
        DownloaderCallback callback;
        
        Task() : id(0) {}
        Status makeStatus() { return {id, url, folder, filename, 0, 0, 0, 0, Status::WAITING}; }
    };
    
private:
    Downloader(); // for singleton
    void runNextTask(); // for download the next task in the waiting list
    void callCallbackSafely(DownloaderCallback callback, int); // make sure the callbacks are executed one by one
    
private:
    int m_lastId;
    
    DownloaderCallback  m_onCompleted;
    std::mutex          m_callbackMutex;
    
    std::queue<Task>    m_waiting;
    std::mutex          m_waitingMutex;
    
    std::unordered_map<int, std::thread::id>    m_running;
    std::mutex                                  m_runningMutex;
    
    int         m_threadCount;
    std::mutex  m_threadCountMutex;
    
    std::unordered_map<int, Status> m_status;
    std::mutex                      m_statusMutex;
    
};

#endif /* defined(__Downloader__Downloader__) */
