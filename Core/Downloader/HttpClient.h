/*****************************************************************************
 Copyright (c) 2014 Xu Xu (Vivo)
 
 http://git.xuxu.name/downloader
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#ifndef __Downloader__HttpClient__
#define __Downloader__HttpClient__

#include "Common.h"
#include <curl/curl.h>
#include "HttpStatus.h"

/**
 @param downloadTotal/downloadNow/uploadTotal/uploadNow in bytes
 */
typedef std::function<void(double,double,double,double)> HttpClientProgressCallback;

/**
 Simple HTTP Client. Only one request at the same time.
 */
class HttpClient
{
public:
    HttpClient();
    HttpClient(std::string agent);
    virtual ~HttpClient();
    
    void setProgressCallback(HttpClientProgressCallback callback);
    
    /** Get web page source and return response code */
    int get(std::string url);
    
    /** Get web page source with arguments and return response code */
    int get(std::string url, std::map<std::string, std::string>& query);
    
    /** Get web page source with customized header and return response code */
    int getWithHeader(std::string url, std::string header);   // TODO: these ...withHeader functions are not well designed
                                                    // need to find a better way to implement the header configurations
    
    /** Post the string map to server and return response code */
    int post(std::string url, const std::map<std::string, std::string>& strings);
    
    /** Post the string map and files to server and return response code*/
    int post(std::string url, const std::map<std::string, std::string>& strings, const std::map<std::string, std::string>& files);
    
    /**
     Post the string map and files to server and return response code.
     allow same keys to implement arrays
     */
    int post(std::string url, const std::multimap<std::string, std::string>& strings, const std::multimap<std::string, std::string>& files);
    
    /** Post the content directly to url */
    int post(std::string url, std::string content);
    
    /** Post the content directly to url with customized headers */
    int postWithHeaders(std::string url, std::string content, std::vector<std::string> headers);
    
    /** Get the response code of the last response */
    int getResponseCode();
    
    /** Get the body of the last response */
    std::string getResponseBody();
    
    /** Download url into folder */
    bool download(std::string url, std::string folder);
    
    /** Download url and rename it to satisfy filepath */
    bool downloadAs(std::string url, std::string filepath);
    
private:
    static size_t writeToFile(void * ptr, size_t size, size_t nmemb, FILE * stream);
    static size_t writeToString(char * ptr, size_t size, size_t nmemb, std::string * sp);
    static int receivedProgress(HttpClient * me, double downTotal, double downNow, double upTotal, double upNow);
    
    void initCURL();
    bool perform();
    
private:
    CURL * m_curl;
    HttpClientProgressCallback m_onProgress;
    
    int m_lastResponseCode;
    std::string m_lastResponseBody;
};

#endif /* defined(__Downloader__HttpClient__) */
